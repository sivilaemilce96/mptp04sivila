import os
productos = []

opcion = 0
def menu():
    opc = int(input("Menu principal \n" +
                    "1_Registrar productos. \n" +
                    "2_Mostrar el listado de productos. \n" +
                    "3_Mostrar productos en el intervalo ingresado. \n" +
                    "4_Agregar stock al inventario. \n" +
                    "5_Eliminar productos sin stock. \n" +
                    "6_Finalizar programa. \n" +
                    "Elija una opción. \n"))
    continuar()
    return opc

def continuar():
    print()
    input('Presione una tecla para continuar.')
    os.system('cls')

def registrarProducto(productos):
    print('Cargar lista de productos.')
    while True:
        codigoProducto = validarEntero('Ingrese el código del producto')
        if codigoProducto > 0:
            producto = buscarProducto(productos, codigoProducto)
            if producto is None:
                break
            else:
                print()
                print('Ya existe un producto con el código ingresado!')
        else:
            print()
            print('El código del producto debe ser un número positivo!')
    descripcionProducto = validarCadena('Ingrese la descripción del producto')    
    while True:
        precioProducto = validarReal('Ingrese el precio del producto')
        if precioProducto > 0:
            break
        else:
            print()
            print('El precio del producto debe ser un número positivo!')    
    while True:
        stockProducto = validarEntero('Ingrese el stock del producto')
        if stockProducto >= 0:
            break
        else:
            print()
            print('El stock del producto debe ser un número positivo!')
    producto = {'Código': codigoProducto,
                'Descripción': descripcionProducto,
                'Precio': precioProducto,
                'Stock': stockProducto}

    AgregarProducto(productos, producto)
    print()
    print('El producto se ha creado de forma satisfactoria.')

def AgregarProducto(productos, producto):
    productos.append(producto)

def buscarProducto(productos, codigoProducto):
    for p in productos:
        if p['Código'] == codigoProducto:
            return p
    return None

def validarEntero(mensaje):
    while True:
        try:
            numero = int(input(f'{mensaje}: '))
            return numero
        except ValueError:
            print('Debe ingresar un número entero!')
        print()

def validarReal(mensaje):
    while True:
        try:
            numero = float(input(f'{mensaje}: '))
            return numero
        except ValueError:
            print('Debe ingresar un número real con "."!')
        print()

def validarCadena(mensaje):
    while True:
        cadena = input(f'{mensaje}: ').strip()
        if len(cadena):
            return cadena
        else:
            print('Debe ingresar una cadena de caracteres con texto!')
        print()

def mostrarProductos(productos):
    print('Listado de productos')
    if len(productos):
            for p in productos:
                print(f"Código: {p['Código']} - Descripción: {p['Descripción']} - Precio: {p['Precio']} - Stock: {p['Stock']}")

def intervaloProductos(productos):
    stockRango = []
    print('Cargar intervalo (desde/hasta).')
    if len(productos):
        while True:
            desde = validarEntero('Ingrese el valor inicial del intervalo')
            if desde >= 0:
                break
            else:
                print()
                print('El valor inicial debe ser un número positivo!')
        while True:
            hasta = validarEntero('Ingrese el valor final del intervalo')
            if hasta >= 0:
                break
            else:
                print()
                print('El valor final debe ser un número positivo!')
    for s in productos:
        if desde <= s['Stock'] <= hasta:
            stockRango.append(s)
    if len(stockRango):
        for s in stockRango:
            print(f"Código: {s['Código']} - Descripción: {s['Descripción']} - Precio: {s['Precio']} - Stock: {s['Stock']}")

def eliminarProducto(productos):
    if len(productos):
        for st in productos:
            if (st['Stock'] == 0):
                productos.remove(st)
                print('Se eliminaron los productos con stock igual a 0.')

def agregarStock(productos):
    print('Agregar stock de productos.')
    if len(productos):
        while True:
            Y = validarEntero('Ingrese el valor del limite de stock disponible')
            if Y >= 0:
                break
            else:
                print()
                print('El valor debe ser un número positivo!')
    for sx in productos:
        if sx['Stock'] < Y:
            while True:
                X = validarEntero('Ingrese la cantidad que quiere agregar al stock del producto')
                if X >= 0:
                    break
                else:
                    print()
                    print('El valor inicial debe ser un número positivo!')
            sx['Stock'] = sx['Stock'] + X
        else:
            print('Este producto no tiene una cantidad de stock menor al valor ingresado!')

os.system('cls')
while opcion != 6:
    opcion = menu()
    if opcion == 1:
        producto = registrarProducto(productos)
    
    if opcion == 2:
        mostrarProductos(productos)

    if opcion == 3:
        intervaloProductos(productos)

    if opcion == 4:
        agregarStock(productos)

    if opcion == 5:
        eliminarProducto(productos)

    if opcion == 6:
        print("Programa finalizado.")